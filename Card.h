//
//  Card.h
//  Poker
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  This class is the basis for the playing cards we will use when we play poker.

#ifndef Card_h
#define Card_h

#include <iostream>
#include <string>
using namespace std;

class Card {
public:
    Card();
	Card(const Card& c) = default;
    Card(string s, int rank);
    int getRank() const;
    string getSuit() const;
	void display() const;
    bool operator < (const Card& a) const;
    
    // Here is our comparison class that will be used as a predicate in our generic algorithm
    class CMP {
    public:
        bool operator()(const Card& a, const Card& b){
            return (a<b);
        }
    };

private:
    string suit;
    int rank;
};


#endif /* Card_h */
