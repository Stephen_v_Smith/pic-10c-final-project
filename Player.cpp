//
//  Player.cpp
//  Casino
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  Creates a player class used for our casino


#include "Player.hpp"

Player::Player():money(1000),bet(0){}

Player::Player(int m,string name):money(m),bet(0),name(name){}

int Player::getMoney() {
    return money;
}

int Player::betMoney(int b){
    if (b<=money && b>=0){
        bet+=b;
        money-=b;
    }
    
    return b;
}

void Player::win(int m) {
    money+=m;
    return;
}

void Player::lose(int m) {
    money-=m;
    return;
}

