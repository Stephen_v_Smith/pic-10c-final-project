//
//  Hand.cpp
//  Poker
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  Implements a poker hand using RAII

#include "Hand.h"
#include <set>


Hand::Hand() :h(nullptr), begin_index(0), hand_size(7) {
	try{
		h = new Card[hand_size];
	}
	catch (exception& e){
		if (h) { delete[] h; }
		h = nullptr;
		throw;
	}
}

Hand::Hand(const Card& c):h(nullptr),begin_index(0),hand_size(7) {
	try{
		h = new Card[hand_size];
		h[begin_index++] = c;
	}
	catch (exception& e){
		if (h) { delete[] h; }
		h = nullptr;
		throw;
	}
	
}

void Hand::takeCard(const Card &c) {
    if (begin_index<hand_size)
        h[begin_index++]=c;
    return;
}

Hand::Hand(const Hand& newh):h(nullptr),begin_index(0),hand_size(7) {
	try{
		h = new Card[hand_size];
		for (size_t i = 0; i < hand_size; ++i){
			h[i] = newh.h[i];
		}
	}
	catch (exception& e){
		if (h) { delete[] h; }
		h = nullptr;
		throw;
	}
}

void Hand::swap(Hand& newh) {
	std::swap(h, newh.h);
	std::swap(begin_index, newh.begin_index);
    std::swap(hand_size,newh.hand_size);
	return;
}

Hand& Hand::operator=(Hand newh) {
	this->swap(newh);
	return *this;
}

Hand::~Hand() {
	delete[] h;
}

void Hand::display() const{
	auto d = [](Card& c)->void{c.display(); };
	Card* begin = &h[0];
	Card* end = begin + hand_size;
	for_each(begin, end, d);
	return;
}

Hand::Hand(Hand&& newh):h(nullptr),hand_size(0),begin_index(0){
    //cout << "Move constructor called\n";
    this->swap(newh);
}

int Hand::isRoyalFlush() const{
    set<int> cardVals;
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    //Check to see if
    for_each(begin, end, [&cardVals](const Card& c)->void{cardVals.insert(c.getRank());});
    int hasTen = static_cast<int>(cardVals.count(10));
    if (hasTen==0) { /*cout << "doesn't have ten\n";*/ return 0;}
    int hasJack = static_cast<int>(cardVals.count(11));
    if (hasJack==0) { /*cout << "doesn't have jack\n";*/ return 0;}
    int hasQueen = static_cast<int>(cardVals.count(12));
    if (hasQueen==0) { /*cout << "doesn't have queen\n";*/ return 0;}
    int hasKing = static_cast<int>(cardVals.count(13));
    if (hasKing==0) {/*cout << "doesn't have king\n";*/ return 0;}
    int hasAce = static_cast<int>(cardVals.count(14));
    if (hasAce==0) {/*cout << "doesn't have ace\n" ;*/ return 0;}
    
    string suitNeeded = isFlush();
    if (suitNeeded=="") return 0;
    
    //cout << suitNeeded << endl;
    
    vector<Card> finalCheck;
    for (size_t i=0;i<hand_size;++i){
        if (h[i].getRank()==10 && h[i].getSuit()==suitNeeded){
            finalCheck.push_back(h[i]);
        }
        else if (h[i].getRank()==11 && h[i].getSuit()==suitNeeded){
            finalCheck.push_back(h[i]);
        }
        else if (h[i].getRank()==12 && h[i].getSuit()==suitNeeded){
            finalCheck.push_back(h[i]);
        }
        else if (h[i].getRank()==13 && h[i].getSuit()==suitNeeded){
            finalCheck.push_back(h[i]);
        }
        else if (h[i].getRank()==14 && h[i].getSuit()==suitNeeded){
            finalCheck.push_back(h[i]);
        }
    }
    if (finalCheck.size()==5)
        return 500;
    
    return 0;
}


int Hand::isStraightFlush() const{
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    string suitNeeded = isFlush();
    if (suitNeeded=="") {/*cout << "Not enough of same suit\n";*/ return 0;}
    //cout << suitNeeded << endl;
    vector<Card> rightCards;
    auto getRightCards = [suitNeeded,&rightCards](const Card& c){
        if (c.getSuit()==suitNeeded){
            rightCards.push_back(c);
        }
    };
    for_each(begin, end, getRightCards);
    Card::CMP comp;
    sort(rightCards.begin(),rightCards.end(),comp);
    //for_each(rightCards.begin(),rightCards.end(),[](const Card& c){c.display();});
    int count = 1;
    int startRank = rightCards[0].getRank();
    int currentRank = startRank+1;
    for (size_t i=1;i<rightCards.size();++i){
        if (rightCards[i].getRank()==currentRank){
            ++currentRank;
            ++count;
        }
        else {
            startRank=rightCards[i].getRank();
            currentRank=startRank+1;
            count=1;
        }
    }
    if (count<5) return 0;
    return startRank;
}

string Hand::isFlush() const{
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    vector<string> cardSuits;
    string suitNeeded;
    for_each(begin, end, [&cardSuits](const Card& c)->void{cardSuits.push_back(c.getSuit());});
    int numDiamonds = static_cast<int>(count(cardSuits.begin(),cardSuits.end(),"Diamonds"));
    int numClubs = static_cast<int>(count(cardSuits.begin(),cardSuits.end(),"Clubs"));
    int numHearts = static_cast<int>(count(cardSuits.begin(),cardSuits.end(),"Hearts"));
    int numSpades = static_cast<int>(count(cardSuits.begin(),cardSuits.end(),"Spades"));
    
    if (numDiamonds>4) {
        suitNeeded="Diamonds";
    }
    else if (numClubs>4) {
        suitNeeded="Clubs";
    }
    else if (numHearts>4) {
        suitNeeded="Hearts";
    }
    else if (numSpades>4) {
        suitNeeded="Spades";
    }
    else {
        return ""; //Not enough of one suit
    }
    return suitNeeded;
    
    
}


void Hand::sortHand() {
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    Card::CMP comp;
    sort(begin, end, comp);
    return;
}


int Hand::isFourOfAKind() const{
    vector<int> cardVals;
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    for_each(begin, end, [&cardVals](const Card& c)->void{cardVals.push_back(c.getRank());});
    
    int most=0;
    size_t index = -1;
    for (size_t i=0;i<4;++i){
        most = static_cast<int>(count(cardVals.begin(), cardVals.end(), cardVals[i]));
        if (most==4) {
            index=i;
            return (h[i].getRank());
        }
        else {
            most=0;
        }
    }
    return 0;
}


vector<int> Hand::isFullHouse() const{
    vector<int> results;
    int threeVal = isThreeOfAKind();
    if (threeVal==0) {
        results.push_back(0);
        results.push_back(0);
        return results;
    }
    
    vector<int> cardVals;
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    auto remainingVals = [&cardVals,threeVal](const Card& c)->void {
        if (c.getRank()!=threeVal){
            cardVals.push_back(c.getRank());
        }
    };
    for_each(begin, end, remainingVals);
    int biggest=0;
    int num = 0;
    for (size_t i=0;i<2;++i){
        num = static_cast<int>(count(cardVals.begin(),cardVals.end(),cardVals[i]));
        if (num==2 && cardVals[i]>biggest){
            biggest=cardVals[i];
        }
    }
    
    if (biggest==0) {
        results.push_back(0);
        results.push_back(0);
        return results;
    }
    results.push_back(threeVal);
    results.push_back(biggest);
    
    return results;
}



int Hand::isThreeOfAKind() const{
    vector<int> cardVals;
    Card* begin = &h[0];
    Card* end = begin + hand_size;
    for_each(begin, end, [&cardVals](const Card& c)->void{cardVals.push_back(c.getRank());});
    
    int most=0;
    size_t index = -1;
    for (size_t i=0;i<5;++i){
        most = static_cast<int>(count(cardVals.begin(), cardVals.end(), cardVals[i]));
        if (most==3) {
            index=i;
            return (h[i].getRank());
        }
    }
    return 0;
}

int Hand::isStraight() const{
    set<int> cardVals;
    for (size_t i=0;i<hand_size;++i){
        cardVals.insert(h[i].getRank());
    }
    vector<int> noRepeatVals(cardVals.size(),0);
    copy(cardVals.begin(),cardVals.end(),noRepeatVals.begin());
    auto descending = [](const int& c1,const int& c2)->bool {
        return c1>c2;
    };
    sort(noRepeatVals.begin(),noRepeatVals.end(),descending);
    
    int count = 1;
    int starting = noRepeatVals[0];
    int current = starting-1;
    
    for (size_t i=1; i<noRepeatVals.size();++i){
        if (noRepeatVals[i]==current){
            ++count;
            current=noRepeatVals[i]-1;
        }
        else {
            count=1;
            starting=noRepeatVals[i];
            current=starting-1;
        }
        if (count==5){
            return (starting-4);
        }
    }
    
    return 0;
}


vector<int> Hand::isTwoPair() const{
    vector<int> finalVals(2,0);
    
    set<int> cardVals;
    vector<int> allCardVals;
    for (size_t i=0;i<hand_size;++i){
        cardVals.insert(h[i].getRank());
        allCardVals.push_back(h[i].getRank());
    }
    if (cardVals.size()>=6){
        return finalVals;
    }
    
    auto descending = [](const int& c1,const int& c2)->bool {
        return c1>c2;
    };
    sort(allCardVals.begin(),allCardVals.end(),descending);
    
    int highVal=0;
    int lowVal=0;
    for (size_t i=0;i<allCardVals.size();++i){
        int c = static_cast<int>(count(allCardVals.begin(), allCardVals.end(), allCardVals[i]));
        if (c==2) {
            if (highVal==0){
                highVal=allCardVals[i];
                finalVals[0]=highVal;
            }
        
            if (lowVal==0 && allCardVals[i]!=highVal){
                lowVal=allCardVals[i];
                finalVals[1]=lowVal;
                return finalVals;
            }
        }
    }
    
    return finalVals;
}

int Hand::isPair() const{
    vector<int> allCardVals;
    for (size_t i=0;i<hand_size;++i){
        allCardVals.push_back(h[i].getRank());
    }
    
    auto descending = [](const int& c1,const int& c2)->bool {
        return c1>c2;
    };
    sort(allCardVals.begin(),allCardVals.end(),descending);
    for (size_t i=0;i<allCardVals.size()-1;++i){
        if (allCardVals[i]==allCardVals[i+1])
            return allCardVals[i];
    }
    return 0;
}


int Hand::isHighCard() const{
    vector<int> allCardVals;
    for (size_t i=0;i<hand_size;++i){
        allCardVals.push_back(h[i].getRank());
    }
    
    auto descending = [](const int& c1,const int& c2)->bool {
        return c1>c2;
    };
    sort(allCardVals.begin(),allCardVals.end(),descending);
    return allCardVals[0];
}

vector<int> Hand::handValue() const {
    vector<int> finalVals(2,0);
    int finalResult = 0;
    finalResult = isHighCard();
    finalVals[0]=finalResult;
    
    int indexing=14;
    int count = 1;
    
    if (isPair()!=0){
        finalResult=isPair()+(indexing*count);
        finalVals[0]=finalResult;
    }
    ++count; //count is 2
    
    if ((isTwoPair()[0])!=0){
        finalVals=isTwoPair();
        finalVals[0]=finalVals[0]+(indexing*count);
    }
    ++count; //count is 3
    
    if (isThreeOfAKind()!=0){
        finalResult=isThreeOfAKind()+(indexing*count);
        finalVals[0]=finalResult;
    }
    ++count; //count is 4
    
    if (isStraight()!=0){
        finalResult=isStraight()+(indexing*count);
        finalVals[0]=finalResult;
    }
    ++count; //count is 5
    
    if (isFlush()!=""){
        finalResult=(indexing*count)+1;
        finalVals[0]=finalResult;
    }
    
    if (isFullHouse()[0]!=0){
        finalVals=isFullHouse();
        finalVals[0]+=(indexing*count);
    }
    ++count; // count is 6
    
    if (isFourOfAKind()!=0){
        finalResult=isFourOfAKind()+(indexing*count);
        finalVals[0]=finalResult;
    }
    ++count; // count is 7
    
    if (isStraightFlush()!=0) {
        finalResult=isStraightFlush()+(indexing*count);
        finalVals[0]=finalResult;
    }
    
    if (isRoyalFlush()!=0){
        finalResult=isRoyalFlush();
        finalVals[0]=finalResult;
    }
    
    return finalVals;
}

void Hand::showTopTwo() const {
    h[0].display();
    h[1].display();
    return;
}

void Hand::showCommunity() const {
    h[2].display();
    h[3].display();
    h[4].display();
    return;
}

void Hand::showTurn() const {
    h[5].display();
    return;
}

void Hand::showRiver() const{
    h[6].display();
    return;
}

Card* Hand::begin() {
    return &h[0];
}

Card* Hand::end() {
    return (begin()+hand_size);
}





