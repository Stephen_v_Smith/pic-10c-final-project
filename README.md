# Final Project #

### Casino! ###

* This project will provide players with the ability to play multiple games as well as test various functionality.

### Poker ###
This option allows the player to play a basic version of Texas Hold 'Em. The player plays against the dealer and can make bets between 0 and the amount of money the player still has remaining. Each player starts with $1000 and can play as long as they have money.	The rules will be made clear as the game is played.


### Concepts Covered ###
* The Hand class manages memory on the heap, so its construction conforms to the **RAII idiom** using the Big 6 (Default Constructor, Constructor, Copy Constructor, Assignment Operator, Destructor, and Move Constructor).
* Repeated use of **lambda functions** for multiple class and member functions.
* Wide use of **generic algorithms** such as:
	* `for_each()`
	* `count()`
	* `sort()`
	* `copy()`
	* `random_shuffle()`

* Created a **functor** that acted as a **predicate** (in the Card class) in order to specify how to sort cards using a generic algorithm.
* In main.cpp, I created my own version of a generic algorithm called `my_generic_alg()` that utilized **templates** as we have discussed in class. This generic algorithm applies a given function to each element of a container.
* **Inheritance:** Poker is derived from an abstract class called Game. This ensures that, when the casino adds more games, we can use Game pointers to call the `play()` function for each derived class without having to specify with a derived class pointer. This will allow us to store such pointers in a container with greater ease. This is an example of **Polymorphism**.
* Used smart pointers, specifically `unique_ptr`, in order to easily manage the Game pointers memory and allow for simple storage in a container, which in this case was a `vector`.
* Made my own **Homemade Forward Iterators** in the Deck class

### Hand Probabilities ###
I also added functionality in the code to determine, via simulation, what the probability is of getting a particular hand with the way the game is set up. Here are the results after 500000 simulations of Texas Hold 'Em with one player and the dealer (only counting the results of the player's hands):

1. Nothing (just using High Card): 0.175722
2. Pair: 0.44071
3. Two Pairs: 0.234624
4. Three of a Kind: 0.053262
5. Straight: 0.04174
6. Flush: 0.030232
7. Full House: 0.021826
8. Four of a Kind: 0.001666
9. Straight Flush: 0.000194
10. Royal Flush: 0.000024

We can see from the simulation that the probability of each hand backs up the ranking of each hand. The likelihood of getting a particular hand decreases as its value increases, which is what we would expect.

You can also compare these probabilities to the results found here: [Wikipedia 7-Card Hand Probabilities](https://en.wikipedia.org/wiki/Poker_probability#Frequency_of_7-card_poker_hands "Poker Probability").

### Files Needed ###
In order to run Casino, you need the following files:

1. Card.h
2. Card.cpp
3. Deck.hpp
4. Deck.cpp
5. Game.cpp
6. Hand.h
7. Hand.cpp
8. Player.hpp
9. Player.cpp
10. Poker.hpp
11. Poker.cpp
12. main.cpp

_Note_: In main.cpp, there is a lot of code that is commented out. These are unit tests that I used to test whether or not different functions were implemented correctly.

_Note_: I would also like to thank Stack Overflow, their code helped me to implement the shuffle function with their code on random number generation that I borrowed.


