//
//  Deck.hpp
//  Casino
//
//  Created by Stephen Smith on 3/21/17.
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  This class makes a full 52 card deck of standard playing cards with functions that allow us to play poker

#ifndef Deck_hpp
#define Deck_hpp

#include <iostream>
#include <vector>
#include "Card.h"
#include <algorithm>
using namespace std;

class Deck {
private:
    vector<Card> d;
    
public:
    Deck();
    Card draw();
	void display();
    void shuffle();
    void diffShuffle(int i);

	//class Iterator;
	
    //Our own homemade deck iterator class
	class Iterator {
	private:
		Deck* parent;
		vector<Card>::iterator v;
		Iterator(Deck* p, vector<Card>::iterator vnew) :parent(p), v(vnew){};
	public:
		//creates an iterator at the top of the deck (beginning of vector)
		Iterator(Deck* p) :parent(p){
			v=p->d.begin();
		}
		friend class Deck;
		Card& operator*(){
			return *v;
		}
		Iterator& operator++(){
			++v;
			return *this;
		}
		Iterator operator++(int unused){
			Iterator tmp(*this);
			++v;
			return tmp;
		}
		bool operator==(const Iterator& rhs){
			return((parent == rhs.parent) && (v == rhs.v));
		}
		bool operator!=(const Iterator& rhs){
			return !(*this == rhs);
		}
	};

	Iterator begin();
	Iterator end();
};

#endif /* Deck_hpp */
