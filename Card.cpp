// Stephen Smith
// UID 504303029
// Implementation of the Card class


#include "Card.h"

Card::Card() {
    suit="";
    rank=0;
}

Card::Card(std::string s,int rank) {
    suit=s;
    this->rank=rank;
}
    
int Card::getRank() const{
    return this->rank;
}
    
std::string Card::getSuit() const {
    return suit;
}
    
bool Card::operator < (const Card& a) const {
    return rank<a.rank;
}

void Card::display() const{
	string val = std::to_string(getRank());
	if (val == "11"){ val = "Jack"; }
	else if (val == "12"){ val = "Queen"; }
	else if (val == "13"){ val = "King"; }
	else if (val == "14"){ val = "Ace"; }

	std::cout << val << " of " << getSuit() << std::endl;

}

