//
//  Poker.hpp
//  Casino
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  Defines everything we need for our poker game

#ifndef Poker_hpp
#define Poker_hpp

#include <iostream>
#include "Game.cpp"
#include "Player.hpp"
#include "Hand.h"
#include "Deck.hpp"
#include <vector>

class Poker : public Game {
public:
    virtual void play();
    Poker(Player p);
    void addPlayer(Player p);
    void communityCardDraws();
    void turnCardDraw();
    void riverCardDraw();
    int playerMoney();
    virtual ~Poker();
    
    //Provides proababilities of getting certain hands after N simulations
    void simulatedHandProbabilities(int N);
    
private:
    Player dealer;
    int numPlayers;
    vector<Player> players;
    vector<Hand> hands;
    Deck playingDeck;
    int pot = 0;
    
};

#endif /* Poker_hpp */
