//
//  Poker.cpp
//  Casino
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  Everything we need to run our poker game

#include "Poker.hpp"

void Poker::play(){
    playingDeck.shuffle();
    for (size_t i = 0;i<hands.size();++i){
        hands[i].takeCard(playingDeck.draw());
        hands[i].takeCard(playingDeck.draw());
        
    }
    cout << "Your initial cards:\n";
    hands[1].showTopTwo();
    
    communityCardDraws();
    turnCardDraw();
    riverCardDraw();
    
    //Final bets
    cout << "\nPlease place your bet: ";
    string sbet;
    getline(cin,sbet);
    int ibet = stoi(sbet);
    players[1].betMoney(ibet);
    pot+=ibet;
    cout << "You have bet $" << ibet << " and you have $" << players[1].getMoney() << " remaining.\n\n";
    
    cout << endl << "Here are the final hands: \n" << "Dealer's Hand:\n";
    hands[0].display();
    cout << endl;
    
    for (size_t i=1;i<hands.size();++i){
        cout << "Player " << i << "'s hand:\n";
        hands[i].display();
    }
    
    vector<int> dealRank=hands[0].handValue();
    vector<int> playerRank=hands[1].handValue();
    
    cout << endl << "Dealer's Hand Rank: " << dealRank[0] << endl << endl;
    
    cout << "Player 1's Hand Rank: " << playerRank[0] << endl;
    
    if (dealRank[0]>playerRank[0]){
        cout << endl << "Dealer Wins :(" << endl;
        cout << "\nYou now have $" << players[1].getMoney() << ".\n";
    }
    else if (playerRank[0]>dealRank[0]){
        cout << endl << "You win! :)" << endl;
        players[1].win(2*pot);
        cout << "\nYou now have $" << players[1].getMoney() << ".\n";
    }
    else {
        int rank1 = playerRank[0];
        
        bool tie;
        if (rank1<29){
            tie=true;
        }
        else if (rank1<43){
            tie=false;
        }
        else if (rank1<72 && rank1>43){
            tie=true;
        }
        else if (rank1<85 && rank1>71){
            tie=false;
        }
        else if (rank1>85){
            tie=true;
        }
        
        if (tie==false) {
            if (playerRank[1]==dealRank[1]){
                tie=true; //The secondary cards are the same too
            }
            else {
                if (playerRank[1]>dealRank[1]){
                    cout << endl << "You win! :)" << endl;
                    players[1].win(2*pot);
                    cout << "\nYou now have $" << players[1].getMoney() << ".\n";
                }
                else {
                    cout << endl << "Dealer Wins :(" << endl;
                    cout << "\nYou now have $" << players[1].getMoney() << ".\n";
                }
            }
        }
        
        if (tie==true){
            cout << endl << "You have tied.\n";
            players[1].win(pot);
            cout << "\nYou now have $" << players[1].getMoney() << ".\n";
        }
        
    }
    
    playingDeck=Deck();
    hands[0]=Hand();
    hands[1]=Hand();
    pot=0;
    return;
}

void Poker::communityCardDraws() {
    cout << "\nPlease place your bet: ";
    string sbet;
    getline(cin,sbet);
    int ibet = stoi(sbet);
    players[1].betMoney(ibet);
    pot+=ibet;
    cout << "You have bet $" << ibet << " and you have $" << players[1].getMoney() << " remaining.\n\n";
    Card communityCard1 = playingDeck.draw();
    Card communityCard2 = playingDeck.draw();
    Card communityCard3 = playingDeck.draw();
    for (size_t i = 0;i<hands.size();++i) {
        hands[i].takeCard(communityCard1);
        hands[i].takeCard(communityCard2);
        hands[i].takeCard(communityCard3);
    }
    cout << "Here are the community cards: \n";
    hands[1].showCommunity();
}

void Poker::turnCardDraw() {
    string sbet;
    int ibet;
    cout << "\nPlease place your bet: ";
    getline(cin,sbet);
    ibet = stoi(sbet);
    players[1].betMoney(ibet);
    pot+=ibet;
    cout << "You have bet $" << ibet << " and you have $" << players[1].getMoney() << " remaining.\n\n";
    Card turn = playingDeck.draw();
    for (size_t i = 0;i<hands.size();++i){
        hands[i].takeCard(turn);
    }
    cout << "Here is the turn card:\n";
    hands[1].showTurn();
    return;
}

void Poker::riverCardDraw() {
    string sbet;
    int ibet;
    cout << "\nPlease place your bet: ";
    getline(cin,sbet);
    ibet = stoi(sbet);
    players[1].betMoney(ibet);
    pot+=ibet;
    cout << "You have bet $" << ibet << " and you have $" << players[1].getMoney() << " remaining.\n\n";
    Card river = playingDeck.draw();
    for (size_t i = 0;i<hands.size();++i){
        hands[i].takeCard(river);
    }
    cout << "Here is the river card:\n";
    hands[1].showRiver();
    cout << endl << "Here is your full hand:\n";
    hands[1].display();
}

Poker::Poker(Player p):numPlayers(1){
    name = "Poker";
    Player dealer;
    players.push_back(dealer);
    players.push_back(p);
    Hand dealHand;
    Hand playerHand;
    hands.push_back(dealHand);
    hands.push_back(playerHand);
    Deck playingDeck;
}

void Poker::addPlayer(Player p) {
    players.push_back(p);
    hands.push_back(Hand());
    return;
}

int Poker::playerMoney() {
    return players[1].getMoney();
}

void Poker::simulatedHandProbabilities(int N){
    double highCard=0;
    double pair=0;
    double twoPairs=0;
    double threeKind=0;
    double straight=0;
    double flush=0;
    double fullHouse=0;
    double fourKind=0;
    double straightFlush=0;
    double royalFlush=0;
    double none=0;
    cout << "Simulated Probabilities for Hands:\n\n";
    for (size_t i = 0;i<N;++i){
        playingDeck.shuffle();
        hands[0].takeCard(playingDeck.draw());
        hands[0].takeCard(playingDeck.draw());
        hands[1].takeCard(playingDeck.draw());
        hands[1].takeCard(playingDeck.draw());
        
        Card communityCard1 = playingDeck.draw();
        Card communityCard2 = playingDeck.draw();
        Card communityCard3 = playingDeck.draw();
        hands[0].takeCard(communityCard1); hands[1].takeCard(communityCard1);
        hands[0].takeCard(communityCard2); hands[1].takeCard(communityCard2);
        hands[0].takeCard(communityCard3); hands[1].takeCard(communityCard3);
        
        Card turn = playingDeck.draw();
        hands[0].takeCard(turn); hands[1].takeCard(turn);
        
        Card river = playingDeck.draw();
        hands[0].takeCard(river); hands[1].takeCard(river);

        vector<int> playerRank = hands[1].handValue();
        //hands[1].display();
        int handVal = playerRank[0];
        
        if (handVal>0 && handVal<15){
            //cout << "High card\n";
            highCard+=1;
        }
        else if (handVal>15 && handVal<29){
            //cout << "pair\n";
            pair+=1;
        }
        else if (handVal>29 && handVal<43){
            //cout << "two pairs\n";
            twoPairs+=1;
        }
        else if (handVal>43 && handVal<57){
            //cout << "three of a kind\n";
            threeKind+=1;
        }
        else if (handVal>57 && handVal<71){
            //cout << "straight\n";
            straight+=1;
        }
        else if (handVal==71){
            //cout << "flush\n";
            flush+=1;
        }
        else if (handVal>71 && handVal<85){
            //cout << "full house\n";
            fullHouse+=1;
        }
        else if (handVal>85 && handVal<99){
            //cout << "four of a kind\n";
            fourKind+=1;
        }
        else if (handVal>99 && handVal<115){
            //cout << "straight flush\n";
            straightFlush+=1;
        }
        else if (handVal==500){
            //cout << "royal flush\n";
            royalFlush+=1;
        }
        else { cout << handVal << endl; none+=1; }
        
        
    
        playingDeck=Deck();
        playingDeck.diffShuffle(static_cast<int>(i));
        hands[0]=Hand();
        //hands[0].display();
        hands[1]=Hand();
        
    }
    //cout << "None: " << none << endl; //used to make sure there are none left over
    cout << "Probabilities:\n";
    cout << "The probability of getting only the value of highest card: " << ((highCard/N)*1.0) << endl;
    cout << "The probability of getting a pair: " << ((pair/N)*1.0) << endl;
    cout << "The probability of getting two pairs: " << ((twoPairs/N)*1.0) << endl;
    cout << "The probability of getting three of a kind: " << ((threeKind/N)*1.0) << endl;
    cout << "The probability of getting a straight: " << ((straight/N)*1.0) << endl;
    cout << "The probability of getting a flush: " << ((flush/N)*1.0) << endl;
    cout << "The probability of getting a full house: " << ((fullHouse/N)*1.0) << endl;
    cout << "The probability of getting four of a kind: " << ((fourKind/N)*1.0) << endl;
    cout << "The probability of getting a straight flush: " << ((straightFlush/N)*1.0) << endl;
    cout << "The probability of getting a royal flush: " << ((royalFlush/N)*1.0) << endl;
    return;
}

Poker::~Poker() {}

