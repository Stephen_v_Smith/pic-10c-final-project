//
//  Hand.h
//  Poker
//
//  Created by Stephen Smith on 3/21/17.
//  Copyright © 2017 Stephen Smith. All rights reserved.
//

#ifndef Hand_h
#define Hand_h
#include <iostream>
#include <utility>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;
#include "Card.h"

class Hand {
    
private:
    Card* h;
    int hand_size=7;
    int begin_index=0;
    int hand_rank=0;
    
    
public:
    //The following are needed for RAII
    Hand();
	virtual ~Hand();
    Hand(const Card& c);
	Hand(const Hand& newh);
	void swap(Hand& newh);
	Hand& operator=(Hand newh);
	Hand(Hand&& newh);
    
    //Takes a card (in Poker, from the deck)
    void takeCard(const Card& c);
    
    //displays the contents of the hand
	void display() const ;
    
    // returns the value of the hand: first value of the vector is between 2 and 114 or 500 (royal flush)
    // 2-14: High Card (each number is the rank of the high card)
    // 16-28: Pair (number minus 14 is the card rank of the pair)
    // 30-42: Two Pair (number minus 28 is the rank of the higher pair)
    // 44-56: Three of a kind (number minus 42 is the rank of card of which you have three of a kind)
    // 58-70: Straight (number minus 56 is the rank of the first card of straight (only functionally goes to 66))
    // 71: Flush
    // 72-84: Full House (number minus 70 is the rank of card of which you have three of a kind)
    // 86-98: Four of a kind (number minus 84 is the rank of card of which you have four of a kind)
    // 100-114: Straight Flush (number minus 98 is the rank of the first card of straight (only functionally goes to 108))
    // 500: Royal Flush
    // if the hand is tied, the second value of the vector helps determine winner if the tie is between these hands:
    // Two pair or full house
    vector<int> handValue() const;
    
    
    // These functions determine whether or not a hand has a certain value
    int isRoyalFlush() const;
    int isStraightFlush() const;
    int isFourOfAKind() const;
    vector<int> isFullHouse() const;
    string isFlush() const;
    int isStraight() const;
    int isThreeOfAKind() const;
    vector<int> isTwoPair() const;
    int isPair() const;
    int isHighCard() const;
    
    // This function sorts a hand using a functor defined in Card and a generic algorithm
    void sortHand();
    
    // Shows cards in a hand or the community cards (flop, turn, and river)
    void showTopTwo() const;
    void showCommunity() const;
    void showTurn() const;
    void showRiver() const;
    
    //Accessor function used to test our move constructor
    int getHandSize() const{ return hand_size; }
    
    // Pointers to the beginning and the end of our hand
    Card* begin();
    Card* end();
    
    
};

#endif
