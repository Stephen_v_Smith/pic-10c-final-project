//
//  Player.hpp
//  Casino
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  Creates a player class used for our casino

#ifndef Player_hpp
#define Player_hpp

#include <iostream>
#include "Hand.h"

class Player {
public:
    Player();
    Player(int m,string name);
    int getMoney();
    void win(int m);
    void lose(int m);
    int betMoney(int b);
    
    
private:
    string name;
    int money;
    int bet;
    
};
#endif /* Player_hpp */
