//
//  main.cpp
//  Casino Game
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  This file provides unit tests for our classes and runs the casino

#include <iostream>
#include <memory>
#include <string>
#include "Card.h"
#include "Hand.h"
#include "Deck.hpp"
#include "Player.hpp"
#include "Poker.hpp"

template<typename It, typename func>
void my_generic_alg(It begin, It end, func f){
	while (begin != end) {
		f(*begin);
		++begin;
	}
	return;
}

int main(int argc, const char * argv[]) {
    
//    std::cout << "THESE ARE UNIT TESTS FOR OUR IMPLEMENTED FUNCTIONALITY\n\n";
//    std::cout << "Testing our Card class:\n\n";
    
/*    Card jackOfHearts("Hearts",11);
    Card* queenOfDiamonds= new Card("Diamonds",12);
	std::string jacksuit = jackOfHearts.getSuit();
	
    std::cout << "The first card has rank " << jackOfHearts.getRank() << " of " << jacksuit << std::endl;
	std::cout << "The second card has rank " << queenOfDiamonds->getRank() << " of " << queenOfDiamonds->getSuit() << std::endl;
    std::cout << std::endl;
    //Should print out "The first card has rank 11 of Hearts"
    //Should also print on a new line "The second card has rank 12 of Diamonds"
    
    std::cout << "The first card is greater than the second card: ";
    if ((jackOfHearts<(*queenOfDiamonds)))
        std::cout << "FALSE" << std::endl;
    else std::cout << "TRUE" << std::endl;
    //Should print "FALSE"
    std::cout << std::endl << std::endl;
 
     //Next function displays our cards in formatted output
     //Should say "Jack of Hearts" and "Queen of Diamonds"

	jackOfHearts.display();
	queenOfDiamonds->display();
	std::cout << std::endl << std::endl;
*/
    /*
     Next we test our Deck class and its functions
    */
/*    std::cout << "Testing our Deck class:\n\n";
    
	Deck d;
	d.display();
	d.shuffle();
	std::cout << std::endl << std::endl << "AFTER THE CARDS WERE SHUFFLED\n\n";
    
	d.display();

 
    // Now we will check to see if our homemade iterator works
 
	cout << endl << endl;
	Deck::Iterator di = d.begin();
	(*di).display();
    cout << endl << endl;
    
    //Using a lambda function to count and make sure we have 52 cards in the deck using our own templatized generic
    // algorithm
	int count = 0;
	my_generic_alg(d.begin(), d.end(), [&count](Card& c)->void{std::cout << ++count << std::endl; });
	cout << "There are a total of " << count << " cards in the deck" << endl << endl;
	
    std::cout << "Testing our Hand class:\n\n";
    Hand h;
	h.takeCard(d.draw());
	h.takeCard(d.draw());
	h.takeCard(d.draw());
	h.takeCard(d.draw());
	h.takeCard(d.draw());
	h.takeCard(d.draw());
	h.takeCard(d.draw());
	h.display();
    
	cout << endl << endl;
    //Now, we must make sure those cards have been removed from the deck
	d.display();
    
    cout << endl << endl;
 */
   
/*    Player p1;
    Poker game(p1);
    //game.play();
    
    Game* listOfGames;
    listOfGames = &game;
    listOfGames->play();
 */
    
/*
    Card rand1("Diamonds",11);
    Card rand2("Spades",14);
    Card hearts10("Hearts",10);
    Card heartsJack("Hearts",11);
    Card heartsQueen("Hearts",12);
    Card heartsKing("Hearts",13);
    Card heartsAce("Hearts",14);
    
    Hand h;
    h.takeCard(hearts10); h.takeCard(heartsJack);
    h.takeCard(heartsQueen); h.takeCard(heartsKing);
    h.takeCard(heartsAce); h.takeCard(rand1); h.takeCard(rand2);
    
    h.display();
    int a = h.isRoyalFlush();
    cout << a << endl;
    
    cout << "\n We are testing our functor predicate and using it to sort our hand\n\n";
    h.sortHand();
    h.display();
    
    cout << endl << endl << "We are now testing if we can detect a straight flush\n\n";
    cout << h.isStraightFlush() << endl;
    
    Hand h2;
    Card rand3("Spades",2); h2.takeCard(rand3);
    Card rand4("Spades",4); h2.takeCard(rand4);
    Card spades8("Spades",8);h2.takeCard(spades8);
    Card spadesJack("Spades",11); h2.takeCard(spadesJack);
    Card spades7("Spades",7); h2.takeCard(spades7);
    Card spades10("Spades",10); h2.takeCard(spades10);
    Card spades9("Spades",9); h2.takeCard(spades9);
    
    cout << endl << h2.isStraightFlush() << endl << endl;
    
    cout << h2.isFourOfAKind() << endl << endl;
    Hand h3;
    Card spades2("Spades",2); h3.takeCard(spades2);
    Card hearts4("Hearts",4); h3.takeCard(hearts4);
    Card clubs8("Clubs",8);h3.takeCard(clubs8);
    Card hearts8("Hearts",8); h3.takeCard(hearts8);
    h3.takeCard(spades9);
    h3.takeCard(spades8);
    Card diamonds8("Diamonds",8); h3.takeCard(diamonds8);
    h3.display();
    
    cout << endl << h3.isFourOfAKind() << endl << endl;
    
    Hand h4;
    h4.takeCard(spades2);
    h4.takeCard(hearts4);
    Card clubs4("Clubs",4);h4.takeCard(clubs4);
    h4.takeCard(hearts8);
    h4.takeCard(spades9);
    h4.takeCard(spades8);
    h4.takeCard(diamonds8);
    h4.display();
    
    cout << endl << h4.isThreeOfAKind() << endl << endl;
    
    cout << endl << h4.isFullHouse()[0] << endl << endl;
    
    //Testing for Straights
    
    cout << "Should return 10: " << h.isStraight() << endl << endl;
    
    //Testing for two pairs
    Hand h5;
    Card diamonds2("Diamonds",2); h5.takeCard(diamonds2);
    Card diamonds4("Diamonds",4); h5.takeCard(diamonds4);
    h5.takeCard(spades8);
    h5.takeCard(diamonds8);
    h5.takeCard(spades7);
    h5.takeCard(spades10);
    Card diamonds10("Diamonds",10); h5.takeCard(diamonds10);
    h5.display();
    cout << "Should return 10 and 8: " << h5.isTwoPair()[0]<< " and " << h5.isTwoPair()[1] << endl << endl;
    
    //Testing for pair
    cout << "Should return 10: " << h5.isPair() << endl;
    
    //Testing for high card
    cout << "Should return 14: " << h.isHighCard() << endl;
    
    cout << endl << endl << endl;
    //Testing hand ranking function
    cout << "Hand value for h (should be 500: royal flush): " << h.handValue()[0] << endl;
    cout << "Hand value for h2 (should be 105: straight flush with starting value 7): " << h2.handValue()[0] << endl;
    cout << "Hand value for h3 (should be 92: 4 of a kind with value 8): " << h3.handValue()[0] << endl;
    cout << "Hand value for h4 (should be 78: full house with 3 8's): " << h4.handValue()[0] << endl;
    cout << "Hand value for h5 (should be 38: two pair with high pair of 10's): " << h5.handValue()[0] << endl;
    
    
    cout << endl << endl << "TESTING PLAY FUNCTION AGAIN" << endl << endl;
 */
    
    //Player stephen(5000,"stephen");
    //Poker pokerGame(stephen);
    //pokerGame.play();
    
    //Test for move constructor
/*    Hand h1(move(Hand()));
    cout << "Should show 7: " << h1.getHandSize() << endl;
*/
    
    
    // ******* Here is the final code that runs the casino **********
    cout << "WELCOME TO THE PIC 10C CASINO!!!\n\n";
    cout << "What is your name? \n";
    string name;
    getline(cin,name);
    cout << "\nHi " << name << "! Thanks for coming to play. You will start with $1000. What would you like to play (currently only poker)?";
    
    Player p1(1000,name);
    Poker* pokerGame = new Poker(p1);
    vector<unique_ptr<Game>> games;
    games.push_back(unique_ptr<Poker>(pokerGame));
    
    cout << "\n\nHere are your game choices. Please select a number and press ENTER. \n";
    cout << "1: " << games[0]->getGameName() << endl;
    string sgame;
    int gameChoice;
    cout << "Your choice: ";
    getline(cin,sgame);
    gameChoice=stoi(sgame);
    cout << endl;
    
    bool keepPlaying = false;
    if (gameChoice==1) keepPlaying=true;
    string cont;
    while (keepPlaying && (games[gameChoice-1]->playerMoney()>0)) {
        games[gameChoice-1]->play();
        cout << "\nWould you like to play again?\n";
        cout << "(yes/no): ";
        getline(cin,cont);
        if (cont=="no"){
            keepPlaying=false;
            cout << "Thanks for playing!\n";
        }
        if (cont=="yes" && (games[gameChoice-1]->playerMoney()<=0)){
            cout << "Sorry, but you have no more money. Thanks for playing and better luck next time!\n";
            keepPlaying=false;
        }
    }


    //The following was used to find the probability of getting certain hands
    
    /*
    Player p2(10,"Stephen");
    Poker pokerGame1(p1);
    pokerGame1.simulatedHandProbabilities(500000);
    */
    
   	
	return 0;
}
