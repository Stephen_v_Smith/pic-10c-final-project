//
//  Deck.cpp
//  Casino
//
//  Created by Stephen Smith, UID 504303029
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  This class makes a full 52 card deck of standard playing cards with functions that allow us to play poker

#include "Deck.hpp"
#include <ctime>

//Creates the standard deck
Deck::Deck() {
	string suits[4] = { "Diamonds", "Clubs", "Hearts", "Spades" };
	int values[13] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
    for (size_t i = 0; i<13;++i){
        for (size_t j = 0;j<4;++j){
            d.push_back(Card(suits[j],values[i]));
        }
    }
}

// Removes the card from the top of the deck and returns its value
Card Deck::draw() {
    Card top = d.back();
    d.pop_back();
    return top;
}

//Uses a generic algorithm to display each card in the deck
void Deck::display() {
	for_each(d.begin(), d.end(), [](Card& c){c.display(); });
}

//Returns our homemade deck iterator pointing to the beginning of the deck
Deck::Iterator Deck::begin() {
	return Deck::Iterator(this, d.begin());
}

//Returns our homemade deck iterator pointing to the end of the deck
Deck::Iterator Deck::end(){
	return Deck::Iterator(this, d.end());
}

// Uses a lambda function and the generic algorithm random shuffle to shuffle the deck
void Deck::shuffle(){
    auto myrandom = [](int i)->int{ return rand()%i;};
    std::srand ( unsigned ( time(0) ) );
	random_shuffle(d.begin(), d.end(),myrandom);
    random_shuffle(d.begin(), d.end(),myrandom);
	return;
}

// Another shuffle function that was used to ensure we got different shuffles when we found probabilities of hands
void Deck::diffShuffle(int i){
    auto myrandom = [i](int j)->int{ return (rand()+i)%j;};
    std::srand ( unsigned ( time(0) ) );
    random_shuffle(d.begin(), d.end(),myrandom);
    random_shuffle(d.begin(), d.end(),myrandom);
    return;
}
