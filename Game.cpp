//
//  Game.cpp
//  Casino
//
//  Created by Stephen Smith on 3/22/17.
//  Copyright © 2017 Stephen Smith. All rights reserved.
//  This is our abstract game class. All derived classes should use public inheritance
//  Derived classes must implement play() and playerMoney()

#include <iostream>

class Game {
public:
    virtual void play()=0; //when used, the game is played in its entirety.
    virtual ~Game() = default;
    virtual std::string getGameName() {
        return name;
    }
    virtual int playerMoney()=0; //returns the amount of money the player has currently
    
protected:
    std::string name;
};
